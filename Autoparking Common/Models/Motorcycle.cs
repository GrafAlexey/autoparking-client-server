﻿namespace Autoparking_Common.Models
{
    public class Motorcycle : Vehicle
    {
        public Motorcycle() : base(Params.Motorcycle, Params.VehicleBalance[Params.Motorcycle])
        {
        }
    }
}

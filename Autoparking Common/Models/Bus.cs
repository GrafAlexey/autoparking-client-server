﻿namespace Autoparking_Common.Models
{
    public class Bus : Vehicle
    {
        public Bus() : base(Params.Bus, Params.VehicleBalance[Params.Bus])
        {
        }
    }
}

﻿using System;
using Newtonsoft.Json;

namespace Autoparking_Common.Models
{
    public class Vehicle
    {
        [JsonProperty("vehicleNumber")]
        public int VehicleNumber { get; private set; }
        [JsonProperty("vehicleType")]
        public string VehicleType { get; private set; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; private set; }
        [JsonProperty("balance")]
        public double Balance { get; private set; }

        public Vehicle()
        {
            
        }
        protected Vehicle(string type, double balance)
        {
            this.VehicleType = type;
            this.Balance = balance;
            this.VehicleNumber = this.GetHashCode();
            this.VehicleId = $"# {this.VehicleNumber} - {this.VehicleType}";
        }
        public void AddBalance(double money)
        {
            if (money >= 0)
            {
                Balance += money;
            }
            else
            {
                throw new Exception($"{nameof(money)} cannot be negative");
            }
        }
        public double WithdrawBalance(double money)
        {
            if (money >= 0)
            {
                if (Balance >= money)
                {
                    Balance -= money;
                    return money;
                }

                return 0d;
            }

            throw new Exception($"{nameof(money)} cannot be negative");
        }
        public override string ToString()
        {
            return VehicleId;
        }
    }
}
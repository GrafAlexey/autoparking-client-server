﻿namespace Autoparking_Common.Models
{
    public class PassengerCar : Vehicle
    {
        public PassengerCar() : base(Params.PassengerCar, Params.VehicleBalance[Params.PassengerCar])
        {
        }
    }
}
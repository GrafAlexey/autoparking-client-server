﻿namespace Autoparking_Common.Models
{
    public class Truck : Vehicle
    {
        public Truck() : base(Params.Truck, Params.VehicleBalance[Params.Truck])
        {
        }
    }
}
﻿namespace Autoparking_Common.RestModels
{
    public class IncomingData
    {
        public Params.VehicleType Vehicle { get; set; }
    }
}
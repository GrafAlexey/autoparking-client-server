﻿using System.Collections.Generic;
using Autoparking_Common.Models;

namespace Autoparking_Common.Contracts
{
    public interface IParking
    {
        double GetBalance { get; }
        double GetBalanceViaMinute { get; }
        int GetFreePlaceCount { get; }
        List<string> GetTransactionsViaMinute { get; }
        string GetTransactionsViaMinuteToString { get; }
        string GetTransactionsToString { get; }
        string GetAllVehiclesToString { get; }
        List<string> GetTransactions { get; }
        List<string> GetAllVehiclesAsStrings { get; }
        List<Vehicle> GetAllVehicles { get; }
        void GetVehicle(int vehicleNumber);
        void AddBalanceToVehicle(int vehicleNumber, double sum);
        void AddVehicle(Params.VehicleType vehicle);
    }
}
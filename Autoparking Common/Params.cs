﻿using System.Collections.Generic;

namespace Autoparking_Common
{
    public static class Params
    {
        public const int Port = 5001;
        public static readonly string LocalHost = $"https://localhost:{Port}/api/";
        public static readonly string VehicleApi = $"{LocalHost}vehicles/";
        public static readonly string BookkeepingApi = $"{LocalHost}bookkeeping/";
        public static readonly string TransactionsApi = $"{BookkeepingApi}transactions/";
        public static readonly string TransactionsViaMinuteApi = $"{BookkeepingApi}transactions/minute";
        public static readonly string BalanceViaMinuteApi = $"{BookkeepingApi}balance/minute";
        public static readonly string BalanceApi = $"{BookkeepingApi}balance";
        public static readonly string AddBalanceApi = $"{BookkeepingApi}vehicle/{{0}}/money/{{1}}";
        public static readonly string FreeplacesApi = $"{VehicleApi}freeplaces";
        public static readonly string GetVehicleApi = $"{VehicleApi}{{0}}";

        public const string Exit = "Exit";
        public const string Motorcycle = "Motorcycle";
        public const string PassengerCar = "Passenger Car";
        public const string Bus = "Bus";
        public const string Truck = "Truck";
        public const int ParkingCapacity = 10;
        public const int ParkingTaxFrequency = 5000;
        public const double ParkingBalance = 0d;
        public const double ParkingPenaltyCoefficient = 2.5d;
        public const double ParkingFine = 2.5d;

        public enum VehicleType
        {
            Motorcycle = 1,
            PassengerCar,
            Bus,
            Truck
        }
        public static readonly IDictionary<string, double> ParkingRates = new Dictionary<string, double>
        {
            {Motorcycle, 1 },
            {PassengerCar, 2 },
            {Bus, 3.5 },
            {Truck, 5 },
        };
        public static readonly IDictionary<string, double> VehicleBalance = new Dictionary<string, double>
        {
            {Motorcycle, 10d },
            {PassengerCar, 20d },
            {Bus, 30d },
            {Truck, 40d },
        };
    }
}

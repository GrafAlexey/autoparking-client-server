﻿using Autoparking_Common.RestModels;
using Microsoft.AspNetCore.Mvc;

namespace Autoparking_Server.Controllers
{
    public class MyBaseController : ControllerBase
    {
        public ActionResult<ClientResponce> ResponseData(object data = null) =>
            new ClientResponce { Data = data ?? "Done" };
    }
}
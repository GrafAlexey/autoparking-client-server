﻿using Autoparking_Common.Contracts;
using Autoparking_Common.RestModels;
using Microsoft.AspNetCore.Mvc;

namespace Autoparking_Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class BookkeepingController : MyBaseController
    {
        private readonly IParking _parking;

        public BookkeepingController(IParking parking)
        {
            _parking = parking;
        }

        [HttpGet("transactions")]
        public ActionResult<ClientResponce> GetTransactions()
        {
            return base.ResponseData(_parking.GetTransactions);
        }

        [HttpGet("vehicle/{vehicleId}/money/{money}")]
        public ActionResult<ClientResponce> AddMoney([FromRoute]int vehicleId, [FromRoute] double money)
        {
            _parking.AddBalanceToVehicle(vehicleId, money);
            return base.ResponseData();
        }

        [HttpGet("transactions/minute")]
        public ActionResult<ClientResponce> GetTransactionsViaMinute()
        {
            return base.ResponseData(_parking.GetTransactionsViaMinute);
        }

        [HttpGet("balance")]
        public ActionResult<ClientResponce> GetBalance()
        {
            return base.ResponseData(_parking.GetBalance);
        }

        [HttpGet("balance/minute")]
        public ActionResult<ClientResponce> GetBalanceViaMinute()
        {
            return base.ResponseData(_parking.GetBalanceViaMinute);
        }
    }
}
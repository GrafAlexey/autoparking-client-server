﻿using Autoparking_Common.Contracts;
using Autoparking_Common.RestModels;
using Microsoft.AspNetCore.Mvc;

namespace Autoparking_Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : MyBaseController
    {
        private readonly IParking _parking;
        public VehiclesController(IParking parking)
        {
            _parking = parking;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<ClientResponce> Get()
        {
            return base.ResponseData(_parking.GetAllVehicles);
        }

        [HttpGet("freeplaces")]
        public ActionResult<ClientResponce> GetFreePlaceCount()
        {
            return base.ResponseData(_parking.GetFreePlaceCount);
        }

        // POST api/values
        [HttpPost]
        public ActionResult<ClientResponce> Post([FromBody] IncomingData incomingData)
        {
            _parking.AddVehicle(incomingData.Vehicle);
            return base.ResponseData();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult<ClientResponce> Delete(int id)
        {
            _parking.GetVehicle(id);
            return base.ResponseData();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Autoparking_Common;
using Autoparking_Common.Models;

namespace Autoparking_Server.ParkingData
{
    public class Bookkeeping
    {
        private readonly List<Transaction> _transactions = new List<Transaction>();
        private readonly IDictionary<int, double> _debtors = new Dictionary<int, double>();
        private static object lockObj = new object();
        private const string FileName = "transaction.txt";
        public Bookkeeping()
        {
            if (File.Exists(FileName))
                InitData();
        }

        private void InitData()
        {
            try
            {
                lock (lockObj)
                {
                    const Int32 bufferSize = 128;
                    using (var fileStream = File.OpenRead(FileName))
                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, bufferSize))
                    {
                        String line;
                        while ((line = streamReader.ReadLine()) != null)
                            _transactions.Add(new Transaction(line));
                    }

                }
            }
            catch (Exception)
            {
                Console.WriteLine($"file {FileName} is not exist");
            }
        }

        public void GetTax(Vehicle vehicle)
        {
            var rate = Params.ParkingRates[vehicle.VehicleType];
            var money = vehicle.WithdrawBalance(rate);
            if (Math.Abs(rate - money) >= 0.00001)
            {
                AddDebtor(vehicle, rate);
            }
            else
            {
                AddMoney(vehicle, rate);
            }
        }

        public bool IsDebtor(Vehicle vehicle)
        {
            if (_debtors.ContainsKey(vehicle.VehicleNumber))
            {
                double dobt = _debtors[vehicle.VehicleNumber];
                double sum = vehicle.WithdrawBalance(dobt);
                if (Math.Abs(sum - dobt) <= 0.000001)
                {
                    _debtors.Remove(vehicle.VehicleNumber);
                    AddMoney(vehicle, sum);
                    return false;
                }

                return true;
            }

            return false;
        }

        public double GetDebt(Vehicle vehicle) =>
            IsDebtor(vehicle) && _debtors.TryGetValue(vehicle.VehicleNumber, out double debt)
                ? debt
                : 0d;

        public void AddMoney(Vehicle vehicle, double money)
        {
            lock (lockObj)
            {
                var transaction = new Transaction
                {
                    Time = DateTime.Now,
                    VehicleType = vehicle.VehicleType,
                    Tax = money,
                    VehicleNumber = vehicle.VehicleNumber
                };
                _transactions.Add(transaction);
                File.AppendAllTextAsync(FileName, transaction.ToCsv);
            }
        }

        public double GetBalance =>
            _transactions.Sum(x => x.Tax);

        public double GetBalanceViaLastMinute =>
            GetDataViaLastMinute()
                .Sum(x => x.Tax);

        public List<string> GetTransactionsViaLastMinute =>
            GetDataViaLastMinute()
                .Select(TransactionToString)
                .ToList();
        public List<string> GetTransactions =>
                _transactions
                    .Select(TransactionToString)
                    .ToList();
        private string TransactionToString(Transaction x) => x.ToString();

        private IEnumerable<Transaction> GetDataViaLastMinute()
        {
            DateTime oneMinuteBefore = DateTime.Now.AddMinutes(-1);

            return _transactions
                 .Where(x => x.Time > oneMinuteBefore);
        }

        private void AddDebtor(Vehicle vehicle, double rate)
        {
            var fine = Params.ParkingFine * rate;
            if (_debtors.ContainsKey(vehicle.VehicleNumber))
            {
                _debtors[vehicle.VehicleNumber] += fine;
            }
            else
            {
                _debtors.TryAdd(vehicle.VehicleNumber, fine);
            }
        }
    }
}

﻿using System;

namespace Autoparking_Server.ParkingData
{
    public class Transaction
    {
        public DateTime Time { get; set; }
        public int VehicleNumber { get; set; }
        public string VehicleType { get; set; }
        public double Tax { get; set; }
        public override string ToString()
        {
            return $"{Time:HH:mm:ss.fff} {VehicleNumber} {Tax} {VehicleType}";
        }

        public string ToCsv => $"{Time.Ticks}, {VehicleNumber}, {Tax}, {VehicleType}\n";

        public Transaction(string csvString = null)
        {
            if (!string.IsNullOrEmpty(csvString))
                InitData(csvString);
        }

        private void InitData(string csvString)
        {
            string[] data = csvString.Split(',');
            if (data.Length != 4)
            {
                throw new Exception("Wrong data format");
            }

            Time = new DateTime(long.Parse(data[0]));
            VehicleNumber = int.Parse(data[1]);
            Tax = double.Parse(data[2]);
            VehicleType = data[3].Trim();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Autoparking_Common;
using Autoparking_Common.Contracts;
using Autoparking_Common.Models;

namespace Autoparking_Server.ParkingData
{
    public class Parking : IParking
    {
        private Timer _aTimer;
        private readonly Bookkeeping _bookkeeping;
        private List<Vehicle> _vehicles = new List<Vehicle>(Params.ParkingCapacity);

        public Parking(Bookkeeping bookkeeping)
        {
            _bookkeeping = bookkeeping;
            SetTimer();
        }

        private void SetTimer()
        {
            _aTimer = new Timer(Params.ParkingTaxFrequency);
            _aTimer.Elapsed += OnEvent;
            _aTimer.AutoReset = true;
            _aTimer.Enabled = true;
        }

        void OnEvent(object s, ElapsedEventArgs e)
        {
            foreach (var vehicle in _vehicles)
            {
                _bookkeeping.GetTax(vehicle);
            }
        }

        public double GetBalance => _bookkeeping.GetBalance;

        public double GetBalanceViaMinute => _bookkeeping.GetBalanceViaLastMinute;

        public void AddBalanceToVehicle(int vehicleNumber, double sum)
        {
            if (_vehicles.FirstOrDefault(x => x.VehicleNumber == vehicleNumber) is Vehicle item)
            {
                item.AddBalance(sum);
                _bookkeeping.IsDebtor(item);
            }
            else
            {
                throw new Exception($"vehicle #{vehicleNumber} does not exist on this parking");
            }
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_vehicles.Count < Params.ParkingCapacity)
            {
                _vehicles.Add(vehicle);
            }
            else
            {
                throw new Exception("on this parking does not have any place");
            }
        }

        public int GetFreePlaceCount => Params.ParkingCapacity - _vehicles.Count;
        public List<string> GetTransactionsViaMinute =>_bookkeeping.GetTransactionsViaLastMinute;
        public List<string> GetTransactions =>_bookkeeping.GetTransactions;

        private string ConvertListToString(List<string> data)
        {
            data.Insert(0, "");
            data.Add("");
            data.Add("");

            return string.Join("\n\r", data);
        }

        public string GetTransactionsViaMinuteToString => ConvertListToString(GetTransactionsViaMinute);
        public string GetTransactionsToString { get; } = null;
        public string GetAllVehiclesToString => ConvertListToString(GetAllVehicles.Select(x=>x.ToString()).ToList());

        public List<Vehicle> GetAllVehicles =>_vehicles;
        public List<string> GetAllVehiclesAsStrings => _vehicles.Select(x=>x.ToString()).ToList();

        public void GetVehicle(int vehicleNumber)
        {
            if (_vehicles.FirstOrDefault(x => x.VehicleNumber == vehicleNumber) is Vehicle vehicle)
            {
                if (!_bookkeeping.IsDebtor(vehicle))
                {
                    _vehicles.Remove(vehicle);
                }
                else
                {
                    throw new Exception($"The vehicle has dept: {_bookkeeping.GetDebt(vehicle)}. You should put extra money on your vehicle balance");
                }
            }
            else
            {
                throw new Exception($"The vehicle: {vehicleNumber} does not contain at the Parking");
            }
        }

        public void AddVehicle(Params.VehicleType vehicle)
        {
            switch (vehicle)
            {
                case Params.VehicleType.Bus: AddVehicle(new Bus());
                    break;

                case Params.VehicleType.Motorcycle: AddVehicle(new Motorcycle());
                    break;

                case Params.VehicleType.PassengerCar: AddVehicle(new PassengerCar());
                    break;

                case Params.VehicleType.Truck: AddVehicle(new Truck());
                    break;

                default: throw new Exception("Not defined type of vehicle");
            }
        }
    }
}

﻿using System;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

using Autoparking_Common.RestModels;

using Newtonsoft.Json;

namespace Autoparking_Client.Infastructure
{
    public class HttpServerClient
    {
        private readonly HttpClient _client =  new HttpClient();

        private async Task<ClientResponce> Execute(Func<Task<HttpResponseMessage>> operation)
        {
            var response = await Execute<ClientResponce>(operation);
            if (!string.IsNullOrEmpty(response.Error))
            {
                throw new Exception(response.Error);
            }

            return response;
        }
        private async Task<T> Execute<T>(Func<Task<HttpResponseMessage>> operation)
        {
            using (HttpResponseMessage response = await operation())
            {
                string responseString = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(responseString);
            }
        }

        public async Task<ClientResponce> GetData(string targetUrl) =>
            await Execute(() => _client.GetAsync(targetUrl));

        public async Task<ClientResponce> DeleteAsync(string targetUrl) =>
            await Execute(() => _client.DeleteAsync(targetUrl));
        public async Task<ClientResponce> SetData(string targetUrl, IncomingData data)
        {
            string body = JsonConvert.SerializeObject(data);
            var httpBody = new StringContent(body, Encoding.UTF8, MediaTypeNames.Application.Json);
            return  await Execute(() => _client.PostAsync(targetUrl, httpBody));
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Autoparking_Client.Extentions;
using Autoparking_Common;
using Autoparking_Common.Contracts;
using Autoparking_Common.Models;
using Autoparking_Common.RestModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Autoparking_Client.Infastructure
{
    public class ParkingClient : IParking
    {
        readonly HttpServerClient _httpServerClient = new HttpServerClient();

        private ClientResponce GetData(string url) =>
            _httpServerClient.GetData(url).Result;
        public double GetBalance =>
            (double)GetData(Params.BalanceApi).Data;

        public double GetBalanceViaMinute =>
            (double)GetData(Params.BalanceViaMinuteApi).Data;

        public int GetFreePlaceCount =>
            (int)(long)GetData(Params.FreeplacesApi).Data;

        public List<string> GetTransactionsViaMinute =>
            GetData(Params.TransactionsViaMinuteApi).Data.ToList<string>();

        public string GetTransactionsViaMinuteToString =>
            GetTransactionsViaMinute.ConvertListToString();

        public string GetTransactionsToString  =>
            GetTransactions.ConvertListToString();

        public string GetAllVehiclesToString =>
            GetAllVehicles.Select(x=>x.ToString()).ToList().ConvertListToString();

        public List<string> GetTransactions =>
            GetData(Params.TransactionsApi).Data.ToList<string>();

        public List<Vehicle> GetAllVehicles =>
           GetData(Params.VehicleApi).Data.ToList<Vehicle>();

        public List<string> GetAllVehiclesAsStrings =>  
            GetAllVehicles.Select(x=>x.ToString()).ToList();

        public void AddBalanceToVehicle(int vehicleNumber, double sum) =>
            GetData(string.Format(Params.AddBalanceApi, vehicleNumber, sum));

        public void AddVehicle(Params.VehicleType vehicle) =>
            _httpServerClient
                .SetData(Params.VehicleApi, new IncomingData {Vehicle = vehicle})
                .Wait();

        public void GetVehicle(int vehicleNumber) =>
            _httpServerClient.DeleteAsync(string.Format(Params.GetVehicleApi, vehicleNumber)).Wait();

    }
}
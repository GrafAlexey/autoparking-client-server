﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Autoparking_Client.Extentions
{
    public static class Utils
    {
        public static string ConvertListToString(this List<string> data)
        {
            data.Insert(0, "");
            data.Add("");
            data.Add("");

            return string.Join("\n\r", data);
        }
        public static List<T> ToList<T>(this object data)
        {
            if(data == null)
                return new List<T>();
            List<T> list = new List<T>();
            foreach (var item in (data as JArray).ToList())
            {
                if (typeof(T).IsValueType || typeof(T) == typeof(string))
                {
                    list.Add(item.ToObject<T>());   
                }
                else
                {
                    list.Add(JsonConvert.DeserializeObject<T>(item.ToString()));   
                }
            }

            return list;
        }

    }
}
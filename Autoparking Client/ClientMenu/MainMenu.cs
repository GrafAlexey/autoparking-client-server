﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autoparking_Client.Infastructure;
using Autoparking_Common;
using Autoparking_Common.Contracts;
using Autoparking_Common.Models;

namespace Autoparking_Client.ClientMenu
{
    public class MainMenu
    {
        static readonly IParking Parking = new ParkingClient();

        const string CurrentParkingBalance = "Current Parking Balance";
        const string TheAmountMmoneyEarnedInTheLastMinute = "The amount of money earned in the last minute";
        const string FindOutTheNumberOfFreeOccupiedParkingPlaces = "Find out the number of free / occupied parking places";
        const string DisplayAllParkingTransactionsForLastMinute = "Display all Parking Transactions for the last minute";
        const string PrintTheEntireHistoryOfTransactions = "Print the entire history of transactions";
        const string DisplayListOfAllVehicles = "Display a list of all Vehicles";
        const string RemovePickUpVehicleFromParking = "Remove / pick up Vehicle from Parking";
        const string CreateDeliverVehicleParking = "Create / deliver Vehicle to Parking";
        const string TopUpTheBalanceOfParticularVehicle = "Top up the balance of a particular Vehicle";
        delegate void CurrentMenu();

        private static readonly IDictionary<string, CurrentMenu> MainMenuAction = new Dictionary<string, CurrentMenu>
        {
            { CurrentParkingBalance, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{CurrentParkingBalance} is {Parking.GetBalance}")  },
            {TheAmountMmoneyEarnedInTheLastMinute, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{TheAmountMmoneyEarnedInTheLastMinute} is {Parking.GetBalanceViaMinute}")  },
            {FindOutTheNumberOfFreeOccupiedParkingPlaces, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit,  null }}, $"Free place is equal {Parking.GetFreePlaceCount}")  },
            {DisplayAllParkingTransactionsForLastMinute, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{DisplayAllParkingTransactionsForLastMinute} {Parking.GetTransactionsViaMinuteToString}")  },
            {PrintTheEntireHistoryOfTransactions, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{PrintTheEntireHistoryOfTransactions} {Parking.GetTransactionsToString}")  },
            {DisplayListOfAllVehicles, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{DisplayListOfAllVehicles} {Parking.GetAllVehiclesToString}")  },
            {CreateDeliverVehicleParking, () => ShowMenu(VehicleCreate, CreateDeliverVehicleParking) },
            {RemovePickUpVehicleFromParking, () => ShowMenu(VehicleDelete(), RemovePickUpVehicleFromParking, VehicleDelete)},
            {TopUpTheBalanceOfParticularVehicle, () => ShowMenu(AddBalance(), TopUpTheBalanceOfParticularVehicle, AddBalance)},
            {Params.Exit, null}
        };

        private static readonly IDictionary<string, CurrentMenu> VehicleCreate = new Dictionary<string, CurrentMenu>()
        {
            { Params.Motorcycle, () => Parking.AddVehicle(Params.VehicleType.Motorcycle) },
            { Params.PassengerCar, () => Parking.AddVehicle(Params.VehicleType.PassengerCar) },
            { Params.Bus, () => Parking.AddVehicle(Params.VehicleType.Bus) },
            { Params.Truck, () => Parking.AddVehicle(Params.VehicleType.Truck) },
            { Params.Exit, null },
        };


        static IDictionary<string, CurrentMenu> VehicleDelete()
        {
            CurrentMenu GetDelegate(Vehicle x) => () => Parking.GetVehicle(x.VehicleNumber);

            IDictionary<string, CurrentMenu> vehicleDelete = Parking.GetAllVehicles
                .ToDictionary(x => x.VehicleId, GetDelegate);

            vehicleDelete.Add(Params.Exit, null);
            return vehicleDelete;
        }
       static double GetMoney()
        {
            do
            {
                try
                {
                    Console.WriteLine("enter extra money");
                    string str = Console.ReadLine();
                    return double.Parse(str);
                }
                catch (Exception)
                {
                    Console.WriteLine("enter just number");
                }

            } while (true);
        }

        static IDictionary<string, CurrentMenu> AddBalance()
        {

            CurrentMenu GetDelegate(Vehicle x) => () => Parking.AddBalanceToVehicle(x.VehicleNumber, GetMoney());

            IDictionary<string, CurrentMenu> vehicleDelete = Parking.GetAllVehicles
                .ToDictionary(x => x.VehicleId, GetDelegate);

            vehicleDelete.Add(Params.Exit, null);
            return vehicleDelete;
        }

        public void Run()
        {
            ShowMenu(MainMenuAction, "main menu");
        }

        static void ShowMenu(IDictionary<string, CurrentMenu> dictionary, string title, Func<IDictionary<string, CurrentMenu>> forUptudate = null)
        {
            List<string> list = dictionary.Keys.ToList();
            string executed = string.Empty;
            do
            {
                if (forUptudate != null)
                {
                    list = forUptudate().Keys.ToList();
                }
                ShowMenu(title, list);
                if (!string.IsNullOrEmpty(executed))
                    Console.WriteLine($"executed #{executed}");

                int number = GetNumberOfMenu(list.Count);
                var str = list[number];
                if (str == Params.Exit)
                    return;
                executed = ExecuteMenu(dictionary, number, str);

            } while (true);
        }

        private static string ExecuteMenu(IDictionary<string, CurrentMenu> dictionary, int number, string str)
        {
            string executed;
            try
            {
                dictionary[str]?.Invoke();
                executed = number.ToString();
            }
            catch (Exception e)
            {
                executed = number + " with Exception:" + (e.InnerException != null ? e.InnerException.Message : e.Message);
            }

            return executed;
        }

        private static int GetNumberOfMenu(int count)
        {
            do
            {
                Console.WriteLine("Choice your variant");
                Console.WriteLine("");
                var str = Console.ReadLine();
                try
                {
                    var result = int.Parse(str);
                    if (result >= 0 && result <= count)
                    {
                        return result;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Enter just number");
                }
                Console.WriteLine("Enter correct number of menu");

            } while (true);
        }

        private static void ShowMenu(string title, ICollection<string> menu)
        {
            Console.Clear();
            Console.WriteLine(title.ToUpper());
            int count = 0;
            foreach (var item in menu)
            {
                Console.WriteLine(count++ + " - " + item);
            }
        }
    }
}
﻿using System;
using Autoparking_Client.ClientMenu;

namespace Autoparking_Client
{
    class Program
    {
        static void Main(string[] args)
        {
            new MainMenu().Run();
        }
    }
}
